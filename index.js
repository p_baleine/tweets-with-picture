var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var request = require('request');
var credentials = require('./credentials');
var Canvas = require('canvas');
var Image = Canvas.Image;

var oauthParameterKeys = [
  'consumer_key',
  'consumer_secret',
  'token',
  'token_secret'
];

var url = 'https://api.twitter.com/1.1/statuses/update_with_media.json';
var oauth = _.pick(credentials, oauthParameterKeys);

fs.readFile(path.join(__dirname, 'image.jpg'), function(err, doraemon) {
  var r = request.post({ url: url, oauth: oauth }, function(e, r, body) {
    console.log(arguments);
  });
  var form = r.form();
  var canvas = new Canvas(235, 215);
  var ctx = canvas.getContext('2d');
  var img = new Image;
  img.src = doraemon;
  ctx.drawImage(img, 0, 0, img.width, img.height);
  // テキスト描画
  ctx.font="20px Georgia";
  ctx.fillText("Hello World!", 10, 50);
  form.append('status', '@sabu_test4 ぼくドラえもん');
  form.append('media[]', canvas.toBuffer());
});
